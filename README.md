# What is Procrastination Timer

For those of us who have the tendancy to get easily distracted.
Welcome to the procrastination timer. Enter a number of minutes. Click Go.
You'll be reminded with a chime every x minutes to get back to work. Enjoy!

It is written using Node.js / HTML / Javascript / CSS / JQuery

## What do I need?

Just a web browser, with Javascript enabled.

## Link

Website can be tested by downloading the files to your computer, or by linking to http://seandev.proctimer.nodejitsu.com