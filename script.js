var targetTime;
var intervalHandle;

var minsField = document.getElementById("mins");
var startButton = document.getElementById("startButton");
var stopButton = document.getElementById("stopButton");
stopButton.disabled=true;
startButton.disabled=false;


minsField.onfocus = function() {
    if (minsField.value == "20") {
        minsField.value = "";
    }
};

minsField.onblur = function() {
    if (minsField.value == "") {
        minsField.value = "20"
    }
};

var countdownlabel = document.getElementById("countdown");

function chime() {
    console.log("Here!");
    document.getElementById("dummy").innerHTML=
        '<embed src="\chimes.wav" hidden="true" autostart="true" loop="false" />';
}

function startClock() {

    if (isNaN(minsField.value) || minsField.value % 1 !== 0 ){
        setCountdownText("Please enter an integer!")
    } else {
    clearInterval(intervalHandle);
    startButton.disabled=true;
    stopButton.disabled=false;
    minsField.disabled=true;

    setCountdownText(formatNum(minsField.value,2) + ":00")
    targetTime = getMSToTarget() + getCurrentMS();

    intervalHandle = setInterval(countdown,1000);
}
}


function countdown() {
    var currentMS = getCurrentMS();
    if (targetTime - getCurrentMS() >= 0) {
        var timeLeft = targetTime - currentMS;

        var minsLeft = (timeLeft/1000)/60;
        var secLeft =  (timeLeft/1000) % 60;

        setCountdownText(formatNum(Math.floor(minsLeft),2) + ":" + formatNum(Math.round(secLeft),2));
    } else {
        setCountdownText("Chime!");
        chime();
        targetTime = getMSToTarget() + currentMS;
    }
}

function formatNum(num, len) {
    var numStr = "" + num;
    while (numStr.length < len) {
        numStr = "0" + numStr;
    }
    return numStr
}


function getMSToTarget() {
    return (parseInt(minsField.value) * 60 * 1000);
}

function getMinToTarget() {
    return minsField.value;
}

function getCurrentMS() {
    var currentTime = new Date();
    return currentTime.getTime();
}


function setCountdownText(countdowntext) {

    countdownlabel.innerHTML = countdowntext;

}

function isFieldEnabled() {

    if (minsField.disabled === true)
        return true;
    else {
        return false;
    }

}


function stopClock() {
    minsField.disabled=true;
    clearInterval(intervalHandle);
    setCountdownText(formatNum(getMinToTarget(),2) + ":00");
    startButton.disabled=false;
    stopButton.disabled=true;
    minsField.disabled=false;
}

function isReturn(target) {

    if (target.keyCode == 13) document.getElementById('startButton').click();
}
